import React from 'react';
import styled from 'styled-components';

const Container =styled.div `
    position: fixed;
    bottom: 0;
    left: 0;
    width: 100%;
`

const Box =styled.div `
background-color:#848181
`

const Link= styled.a`color:#fff`

const Footer = () => {
    return (
        <Container >
            <Box className="navbar navbar-expand-lg">
                <span class="navbar-text">&#169; 2023 Company </span>
                <ul className="navbar-nav ml-auto">
                    <li className="nav-item">
                        <Link className="nav-link" href="/"> About</Link>
                    </li>
                    <li className="nav-item">
                        <Link className="nav-link" href="/"> Terms of use</Link>
                    </li>
                    <li className="nav-item">
                        <Link className="nav-link" href="/"> Contact</Link>
                    </li>
                </ul>
            </Box>
        </Container>
    );
};

export default Footer;
