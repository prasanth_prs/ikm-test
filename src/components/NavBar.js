import React from 'react';

const NavBar = () => {
    return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark justify-content-space-between">
            <a className="navbar-brand" href="/"><i className='fa fa-qq text-success'></i></a>
            <a className="navbar-brand" href="/">Grave Site Management</a>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarText">
                <ul className="navbar-nav ml-auto">
                    <li className="nav-item active">
                        <li className="nav-item dropdown">
                            <a className="nav-link dropdown-toggle" href="/" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i className='fa fa-th-large color-white'> </i> Maintain
                            </a>
                            <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a className="dropdown-item" href="/">Page 1</a>
                                <a className="dropdown-item" href="/">Page 2</a>
                                <a className="dropdown-item" href="/">Page 3</a>
                            </div>
                        </li>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="/"><i className='fa fa-usd'></i> Payments</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="/"><i className='fa fa-file-text'></i> Reports</a>
                    </li>
                </ul>
                <ul className="navbar-nav ml-auto">
                    <li className="nav-item">
                        <a className="nav-link" href="/"><i className='fa fa-search'></i></a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="/"><i className='fa fa-cog'></i></a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="/"><i className='fa fa-question-circle-o'></i></a>
                    </li>
                    <li className="nav-item active">
                        <li className="nav-item dropdown">
                            <a className="nav-link dropdown-toggle" href="/" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                               John Snow
                            </a>
                            <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a className="dropdown-item" href="/">Page 1</a>
                                <a className="dropdown-item" href="/">Page 2</a>
                                <a className="dropdown-item" href="/">Page 3</a>
                            </div>
                        </li>
                    </li>
                </ul>
            </div>
        </nav>
    );
};

export default NavBar;
