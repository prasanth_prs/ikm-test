import MainLander from "./modules/MainLander";

function App() {
  return (
    <div className="App">
      <MainLander/>
    </div>
  );
}

export default App;
