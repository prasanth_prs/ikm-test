import Footer from "../components/Footer";
import NavBar from "../components/NavBar";
import Payment from "./payments/Payment";

const MainLander=()=>{

    return(
        <div className="justify-content-center">
            <NavBar/>
            <Payment/>
            <Footer/>
        </div>
    )
}

export default MainLander;