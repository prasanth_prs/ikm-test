export const data = [
    {
        receipt: 1234,
        status: "Unposted",
        date: "01 / 23 / 2023",
        site: "01-1001-03.0",
        by: "Lorraine & Russ Stewart",
        payments: 2,
        total: 50
    },
    {
        receipt: 1233,
        status: "Unposted",
        date: "01 / 23 / 2023",
        site: "01-1001-01.0",
        by: "Marley Aminoff",
        payments: 3,
        total: 10
    },
    {
        receipt: 1232,
        status: "Unposted",
        date: "01 / 23 / 2023",
        site: "01-1001-03.1",
        by: "Emery Dias",
        payments: 1,
        total: 50
    },
    {
        receipt: 1231,
        status: "Posted",
        date: "01 / 23 / 2023",
        site: "01-1001-05.0",
        by: "Zaire Levin",
        payments: 1,
        total: 50
    },
    {
        receipt: 1230,
        status: "Posted",
        date: "01 / 22 / 2023",
        site: "01-1002-04.0",
        by: "Hanna Geidt",
        payments: 2,
        total: 50
    },
    {
        receipt: 1229,
        status: "Posted",
        date: "01 / 22 / 2023",
        site: "01-1002-03.0",
        by: "Haylie Herwitz",
        payments: 1,
        total: 50
    },
    {
        receipt: 1228,
        status: "Posted",
        date: "01 / 22 / 2023",
        site: "01-1001-02.0",
        by: "Desirae Levin",
        payments: 1,
        total: 50
    },
    {
        receipt: 1227,
        status: "Posted",
        date: "01 / 17 / 2023",
        site: "01-1002-01.1",
        by: "Lincoln Philips",
        payments: 1,
        total: 50
    },
    {
        receipt: 1226,
        status: "Posted",
        date: "01 / 17 / 2023",
        site: "01-1002-03.2",
        by: "Desirae Levin",
        payments: 1,
        total: 50
    },
    {
        receipt: 1225,
        status: "Posted",
        date: "01 / 16 / 2023",
        site: "01-1001-03.3",
        by: "Lincoln Philips",
        payments: 1,
        total: 50
    }
    
]