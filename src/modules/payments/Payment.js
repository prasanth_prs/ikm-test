import styled from 'styled-components';
import PaymentList from "./PaymentList";
import TopBar from "./TopBar"
import {data} from './mockData'

const Container = styled.div`
padding:10px 30px;
background-color:#ebf7f5a3;
margin-bottom:20px;
`;

const Payment=()=>{
    return(
        <Container>
            <TopBar/>
            <PaymentList list={data}/>
        </Container>
    )
}

export default Payment;