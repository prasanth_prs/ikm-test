import styled from 'styled-components';

const Container = styled.div` padding:30px`;
const Table = styled.table` padding:30px`;
const TableHead = styled.thead``;
const TableBody = styled.tbody``;
const TableRow = styled.tr`
margin:20px;
padding:8px;
border:1px solid black;
border-radius:5px;
box-shadow:
`;
const TableH = styled.th``;
const TableD = styled.td``;
const TableDUP = styled.span`background-color:yellow; border-radius:10px; padding:10px 20px;`;
const TableDP = styled.span`background-color:green; border-radius:10px; color:#fff;padding:10px 20px;`;

const PaymentList = ({ list = [] }) => {

    return (<>
        <Container className='table-responsive'>
            <Table className="table">
                <TableHead>
                    <TableRow>
                        <TableH><input type="checkbox" /></TableH>
                        <TableH>Receipt</TableH>
                        <TableH>Status</TableH>
                        <TableH>Date Paid</TableH>
                        <TableH>Grave Site</TableH>
                        <TableH>Paid By</TableH>
                        <TableH>No. of Payment</TableH>
                        <TableH>Total</TableH>
                        <TableH><i className='fa fa-th-list'></i></TableH>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {list.map((item, i) => (
                        <TableRow key={i}>
                            <TableD><input type="checkbox" /></TableD>
                            <TableD>{item.receipt}</TableD>
                            <TableD>
                                {item.status === "Unposted" ?
                                    <TableDUP>{item.status}</TableDUP> :
                                    <TableDP>{item.status}</TableDP>}
                            </TableD>
                            <TableD>{item.date}</TableD>
                            <TableD>{item.site}</TableD>
                            <TableD>{item.by}</TableD>
                            <TableD>{item.payments}</TableD>
                            <TableD><i className='fa fa-usd'></i>{item.total}</TableD>
                            <TableD><i className='fa fa-ellipsis-h'></i></TableD>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
            <div className='container-fluid d-flex row mx-3 w-80'>
                <select className="custom-select col-sm-3">
                    <option selected>Display 10/page</option>
                    <option value="1">Display 10/page</option>
                    <option value="2">Display 20/page</option>
                    <option value="3">Display 50/page</option>
                    <option value="4">Display 100/page</option>
                </select>
                <nav aria-label="Page navigation example" className='ml-auto mr-3'>
                    <ul className="pagination">
                        <li className="page-item">
                            <a className="page-link" href="/"style={{backgroundColor:"green",color:"#fff"}}>
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                        </li>
                        <li className="page-item"><a className="page-link" href="/">1</a></li>
                        <li className="page-item"><a className="page-link" href="/">2</a></li>
                        <li className="page-item"><a className="page-link" href="/">.</a></li>
                        <li className="page-item"><a className="page-link" href="/">.</a></li>
                        <li className="page-item"><a className="page-link" href="/">.</a></li>
                        <li className="page-item"><a className="page-link" href="/">23</a></li>
                        <li className="page-item"><a className="page-link" href="/">24</a></li>
                        <li className="page-item">
                            <a className="page-link" href="/" style={{backgroundColor:"green",color:"#fff"}}>
                                <span aria-hidden="true">&raquo;</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </Container>
    </>)
}

export default PaymentList;