import styled from 'styled-components';

const Container = styled.div`
margin:30px;
`;

const Div = styled.div`
display:flex;
`;

const DivNotify = styled.div`
display:flex;
background-color:#222;
align-items:center;
height:70px;
border-radius:6px;
padding:10px;
margin-top:30px;
`;

const Info1=styled.p`
color:#fff;
font-weight:bold;
margin:0px;
`;

const Info2=styled.p`
color:#fff;
margin:0px;
`;

const Button=styled.button`
border-radius:30px;
padding:10px 30px;
`;


const TopBar = () => {

    return (
        <Container>
            <Div>
                <h2>Payments</h2>
                <form className="form-inline ml-auto">
                    <div className="input-group">
                        <div className="input-group-prepend">
                            <span className="input-group-text" id="basic-addon1"><i className='fa fa-search'></i></span>
                        </div>
                        <input type="text" className="form-control" placeholder="Search by receipt, grave site, payor " aria-label="Search" aria-describedby="basic-addon1" />
                    </div>
                    <Button className="btn btn-secondary ml-2 my-sm-0" type="submit"><i className='fa fa-filter'></i>Filter</Button>
                    <Button className="btn btn-success ml-2" type="submit">+ Payment</Button>
                </form>
            </Div>
            <DivNotify>
                <Div>
                    <Info1>3 Payments may not have been posted</Info1>
                </Div>
                <Div className='ml-auto'>
                    <Info2><i className='fa fa-list'></i>  Preview Posting Result</Info2>
                    <Info2><i className='fa fa-close ml-4'></i></Info2>
                </Div>
            </DivNotify>
        </Container>
    )
}

export default TopBar;